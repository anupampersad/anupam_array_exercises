const items = [1, 2, 3, 4, 5, 5]

function each(elements,cb){

    let temp = []

    if (typeof(cb)!=='function'){
        console.log('Please pass a call back function as second parameter')
        return
    }
   
    
    if (Array.isArray(elements)){
        for ( let i=0; i<elements.length ; i++){
            temp.push(cb(elements[i],i))
        }
    
        return temp
    }

    else{
        return undefined
    }
    
}

module.exports = each
