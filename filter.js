function filter(elements, cb, value) {

    if (typeof(cb)!=='function'){
        console.log('Please pass a call back function as second parameter')
        return
    }
   
    let arr = []
    if (Array.isArray(elements)) {
        for (let i = 0; i < elements.length; i++) {

            if (cb(elements[i], value)) {
                arr.push(elements[i])
            }

        }

        return arr
    }
    else{
        return undefined
    }
}


module.exports = filter



