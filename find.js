function find(elements, cb, value) {


    if (typeof(cb)!=='function'){
        console.log('Please pass a call back function as second parameter')
        return
    }

    let flag = false
    if (Array.isArray(elements)) {
        for (let i = 0; i < elements.length; i++) {
            flag = cb(elements[i], value)
            if (flag === true) {
                return value

            }

            
        }

        return undefined

    }
}

module.exports = find