function flatten(elements){

    let temp = []
    if (Array.isArray(elements)) {

        
        for (let i = 0; i < elements.length; i++) {
        
            if (Array.isArray(elements[i])){
                temp.push(...flatten(elements[i]))
            }
            else{
                temp.push(elements[i])
                
            }
            
        }

        
    }

    return temp
}


module.exports = flatten