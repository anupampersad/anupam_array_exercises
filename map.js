const items = [1, 2, 3, 4, 5, 5]

function map(elements,cb){

    if (typeof(cb)!=='function'){
        console.log('Please pass a call back function as second parameter')
        return
    }

    let temp = []
    
    if (Array.isArray(elements)){
        for ( let i=0; i<elements.length ; i++){
            temp.push(cb(elements[i]))
        }
    
        return temp
    }

    else{
        return undefined
    }
    
}


module.exports = map
