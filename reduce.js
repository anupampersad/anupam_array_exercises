function reduce(elements, cb, startingValue) {

    let res = 0;

    let iniitialValue;
    if (startingValue == undefined) {

        iniitialValue = elements[0]

    } else {

        iniitialValue = startingValue
    }

    if (Array.isArray(elements)) {

        
        for (let i = 0; i < elements.length; i++) {
            
            res += cb(iniitialValue, elements[i], i)
            
        }
    }

    return res

}


module.exports = reduce