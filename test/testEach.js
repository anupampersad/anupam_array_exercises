const each = require('../each')


const items = [1, 2, 3, 4, 5, 5]

// Function used as callback
function print(element,index){

    return {index,element}

}

console.log(each(items,print))