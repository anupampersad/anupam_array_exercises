const reduce = require('../reduce')


// Function used as callback
function cb(startingValue, element,counter) {
 
    let val = 0
    if (counter == 0){

        val = startingValue + element

    }
    else{

        val += element

    }

    return val
}



const items = [1, 2, 3, 4, 5, 5]


console.log(reduce(items,cb,0))